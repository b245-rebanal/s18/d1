



// let nickname = "Lito"



function printInput(){
	let nickname = prompt("Enter your nickname: ");
	console.log("Hi, " +nickname);
}

// printInput();



//Parameter passing


function printName(name){
	console.log("My name is "+name);
}

printName("Angelito");

printName("tolits");
printName();

//[SECTION] Parameters and Arguments
	//Parameter
		// "name" is called a paramenter
		//acts as a named variable/container that only exists inside a function.
		//it is used to store information that only exists 

function checkDivisibilityBy8(num){
        let remainder = num % 8;
        console.log("The remainder of " + num + " divided by 8 is: " + remainder);
        let isDivisibleBy8 = remainder === 0;
        console.log("Is " + num + " divisible by 8?");
        console.log(isDivisibleBy8);
    }

checkDivisibilityBy8(32);


// [SECTION] Function as Arguments

	// function parameters canalso accepts other function as arguments.
	//Some complex fuctions use other functions as arguments to perform more complicated results.

	
	function argumentFunc(){
		console.log("Thi function was pased as an argument before the message was printed")
	}

	function invokeFunc(argFunc){
		// console.log(argFunc);
		argFunc();
	}

	invokeFunc(argumentFunc);

//[SECTION] Multiple Parameters

	// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

	function createFullName(firstName ,lastName, middleName){
		console.log(firstName+ " " +middleName+ " " +lastName)
	}

	createFullName("Juan", "Enye", "Dela Cruz")
	createFullName("Juan","Dela Cruz")
	createFullName("Juan","Enye","Dela Cruz","Jr")

	// Using variables as arguments

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

// [SECTION] Return Statement
	//alloo

	function returnFullName(firstName, middleName, lastName){
		console.log(firstName +" "+ middleName +" "+ lastName);
		return firstName +" "+ middleName +" "+ lastName
		
	}

	let completeName = returnFullName("jeffrey", "Smith", "Doe");
	console.log(completeName);

	console.log("I am "+ completeName +", I live in Quzon");
	console.log( "my complete name is "+completeName);

	function checkDivisibilityBy5(num){
        let rem = remainder(num);
        console.log("The remainder of " + num + " divided by 5 is: " + rem);
        let isDivisibleBy8 = remainder === 0;
        console.log("Is " + num + " divisible by 5?");
        console.log(isDivisibleBy8);
    }

    function remainder(num){
    	return num % 5;
    }

    checkDivisibilityBy5(25);

    function returnAddress(city, country){
    	let fullAddress = city+", "+country;
    	return fullAddress;
    }

    let myAddress = returnAddress("cebu city", "cebu");
    console.log(myAddress);

    function printPlayerInfo(username, level, job){
    	console.log("Username: " +username);
    	console.log("Username: " +level);
    	console.log("Username: " +job)
    };

    let user = printPlayerInfo("knight_white", 95,"Paladin");
    console.log(user);